/*
 * Purple - Internet Messaging Library
 * Copyright (C) Pidgin Developers <devel@pidgin.im>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <https://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <purple.h>
#include <libpwmd.h>

#ifndef GETTEXT_PACKAGE
#ifndef _
#define _(s) s
#endif
#ifndef N_
#define N_ _
#endif
#endif

#define LIBPWMD_ID		"libpwmd"
#define LIBPWMD_NAME		N_("libpwmd")
#define LIBPWMD_DESCRIPTION	N_("Keyring for libpwmd.")
#define LIBPWMD_URL		"https://gitlab.com/bjk/libpurple-pwmd"
#define LIBPWMD_PLUGIN_VERSION	PACKAGE_VERSION
#define LIBPWMD_DOMAIN		(g_quark_from_static_string ("libpwmd"))

#define SETTINGS_SCHEMA_ID	"im.pidgin.Purple.Credentials.libpwmd"
#define PREF_SOCKET		"socket"
#define PREF_SOCKET_ARGS	"socketargs"
#define PREF_DATAFILE		"datafile"
#define PREF_ROOT		"root"

#define PURPLE_TYPE_LIBPWMD	(purple_libpwmd_get_type ())

G_DECLARE_FINAL_TYPE (PurpleLibPwmd, purple_libpwmd,
		      PURPLE, LIBPWMD, PurpleCredentialProvider)
struct _PurpleLibPwmd
{
  PurpleCredentialProvider parent;
  pwm_t *pwm;
};

G_DEFINE_DYNAMIC_TYPE (PurpleLibPwmd, purple_libpwmd,
		       PURPLE_TYPE_CREDENTIAL_PROVIDER)
/******************************************************************************
 * Globals
 *****************************************************************************/
static PurpleCredentialProvider *instance = NULL;

/******************************************************************************
 * Helpers
 *****************************************************************************/
static void
purple_libpwmd_set_task_error_from_rc (GTask *task, gpg_error_t rc)
{
  g_task_return_new_error (task, LIBPWMD_DOMAIN, rc, "%s", gpg_strerror (rc));
}

static gchar *
get_root_element ()
{
  GSettings *settings = g_settings_new_with_backend(SETTINGS_SCHEMA_ID,
                                         purple_core_get_settings_backend());
  gchar *root = g_settings_get_string (settings, PREF_ROOT);
  gchar *p;

  for (p = root; p && *p; p++)
    {
      if (*p == '^')
        *p = '\t';
    }

  if (root && !*root)
    {
      g_free (root);
      root = NULL;
    }

  g_object_unref(settings);
  return root;
}

static gchar *
build_element_path (PurpleAccount *account, const gchar *element)
{
  gchar *result = NULL, *root = NULL;

  root = get_root_element ();
  result = pwmd_strdup_printf ("%s%s%s\t%s\t%s", root ? root : "",
			       root ? "\t" : "",
			       purple_account_get_protocol_id (account) + 5,
			       purple_account_get_username (account),
			       element);
  g_free (root);
  return result;
}

static gpg_error_t
connect_and_open (PurpleAccount *account, pwm_t **result)
{
  gpg_error_t rc;
  pwm_t *pwm;
  gchar **args = NULL;
  gchar *line;
  gint len;
  GSettings *settings = NULL;
  settings = g_settings_new_with_backend(SETTINGS_SCHEMA_ID,
                                         purple_core_get_settings_backend());
  gchar *sock = g_settings_get_string (settings, PREF_SOCKET);
  gchar *filename = g_settings_get_string (settings, PREF_DATAFILE);

  rc = pwmd_new ("pidgin", &pwm);
  if (rc)
    {
      g_free (sock);
      g_free (filename);
      g_object_unref(settings);
      return rc;
    }

  rc = pwmd_setopt (pwm, PWMD_OPTION_LOCK_ON_OPEN, 0);
  if (!rc)
    rc = pwmd_setopt (pwm, PWMD_OPTION_LOCK_TIMEOUT, 100);
  if (rc)
    {
      g_free (sock);
      g_free (filename);
      return rc;
    }

  line = g_settings_get_string (settings, PREF_SOCKET_ARGS);
  if (line && *line)
    args = g_strsplit (line, ",", 0);

  g_free (line);
  len = args ? g_strv_length (args) : 0;
  sock = sock && *sock ? sock : NULL;
  filename = filename && *filename ? filename : NULL;
  rc = pwmd_connect (pwm, sock,
		     len > 0 ? args[0] : NULL, len > 1 ? args[1] : NULL,
		     len > 2 ? args[2] : NULL, len > 3 ? args[3] : NULL,
		     len > 4 ? args[4] : NULL, len > 5 ? args[5] : NULL,
		     len > 6 ? args[6] : NULL, len > 7 ? args[7] : NULL);
  if (args)
    g_strfreev (args);

  if (!rc)
    rc = pwmd_open (pwm, filename, NULL, NULL);

  if (rc)
    pwmd_close (pwm);
  else
    *result = pwm;

  g_free (sock);
  g_free (filename);
  g_object_unref(settings);
  return rc;
}

static gpg_error_t
get_element (PurpleCredentialProvider *provider, PurpleAccount *account,
             const gchar *element, gchar **result)
{
  gpg_error_t rc;
  gchar *tmp = NULL;
  gchar *path = NULL;

  PurpleLibPwmd *tp = PURPLE_LIBPWMD (provider);
  rc = connect_and_open (account, &tp->pwm);
  if (rc)
    return rc;

  path = build_element_path (account, element);
  if (!path)
    return GPG_ERR_ENOMEM;

  rc = pwmd_command (tp->pwm, &tmp, NULL, NULL, NULL, "GET %s", path);
  pwmd_free (path);

  if (!rc)
    {
      *result = g_strdup (tmp);
      pwmd_free (tmp);
    }

  return rc;
}

/******************************************************************************
 * PurpleCredentialProvider Implementation
 *****************************************************************************/
static void
purple_libpwmd_read_password_async (PurpleCredentialProvider *provider,
				    PurpleAccount *account,
				    GCancellable *cancellable,
				    GAsyncReadyCallback callback,
				    gpointer data)
{
  g_return_if_fail (PURPLE_IS_CREDENTIAL_PROVIDER (provider));
  g_return_if_fail (PURPLE_IS_ACCOUNT (account));

  GTask *task = g_task_new (G_OBJECT (provider), cancellable, callback, data);
  gchar *result = NULL;
  gpg_error_t rc = get_element (provider, account, "password", &result);

  if (!rc)
    g_task_return_pointer (task, result, g_free);
  else
    purple_libpwmd_set_task_error_from_rc (task, rc);

  g_object_unref (task);
}

static gchar *
purple_libpwmd_read_password_finish (PurpleCredentialProvider *provider,
				     GAsyncResult *result, GError **error)
{
  g_return_val_if_fail (PURPLE_IS_CREDENTIAL_PROVIDER (provider), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  PurpleLibPwmd *tp = PURPLE_LIBPWMD (provider);
  pwmd_close (tp->pwm);
  tp->pwm = NULL;

  return g_task_propagate_pointer (G_TASK (result), error);
}

static gpg_error_t
inquire_cb (void *user, const gchar *keyword, gpg_error_t rc, gchar **data,
            size_t *len)
{
  if (rc)
    return rc;

  *data = user;
  *len = strlen ((gchar *)(user));
  return GPG_ERR_EOF;
}

static void
write_clear_common (PurpleCredentialProvider *provider,
                    PurpleAccount *account,
                    const gchar *password,
                    GCancellable *cancellable,
                    GAsyncReadyCallback callback,
                    gpointer data)
{
  GTask *task = NULL;
  gchar *root, *path;
  gpg_error_t rc = 0;
  PurpleLibPwmd *tp = PURPLE_LIBPWMD (provider);

  task = g_task_new (G_OBJECT (provider), cancellable, callback, data);
  root = build_element_path (account, "password");
  if (!root)
    {
      purple_libpwmd_set_task_error_from_rc (task, gpg_error (GPG_ERR_ENOMEM));
      g_object_unref (task);
      return;
    }

  if (password)
    path = pwmd_strdup_printf ("%s\t%s", root, password);
  else // clear password
    path = pwmd_strdup_printf ("%s", root);

  pwmd_free (root);

  if (!path)
    rc = gpg_error (GPG_ERR_ENOMEM);
  else
    rc = connect_and_open (account, &tp->pwm);

  if (rc)
    {
      pwmd_free (path);
      purple_libpwmd_set_task_error_from_rc (task, rc);
      g_object_unref (task);
      return;
    }

  if (password)
    rc = pwmd_command(tp->pwm, NULL, NULL, inquire_cb, path, "%s", "STORE");
  else
    rc = pwmd_command(tp->pwm, NULL, NULL, NULL, NULL, "DELETE %s", path);

  pwmd_free (path);
  if (!rc)
    rc = pwmd_save (tp->pwm, NULL, NULL, NULL);
  else
    purple_libpwmd_set_task_error_from_rc (task, rc);

  g_object_unref (task);
}

static void
purple_libpwmd_write_password_async (PurpleCredentialProvider *provider,
				     PurpleAccount *account,
				     const gchar *password,
				     GCancellable *cancellable,
				     GAsyncReadyCallback callback,
				     gpointer data)
{
  write_clear_common (provider, account, password, cancellable, callback, data);
}

static gboolean
purple_libpwmd_write_password_finish (PurpleCredentialProvider *provider,
				      GAsyncResult *result, GError **error)
{
  g_return_val_if_fail (PURPLE_IS_CREDENTIAL_PROVIDER (provider), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  PurpleLibPwmd *tp = PURPLE_LIBPWMD (provider);
  pwmd_close (tp->pwm);
  tp->pwm = NULL;
  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
purple_libpwmd_clear_password_async (PurpleCredentialProvider *provider,
				     PurpleAccount *account,
				     GCancellable *cancellable,
				     GAsyncReadyCallback callback,
				     gpointer data)
{
  write_clear_common (provider, account, NULL, cancellable, callback, data);
}

static gboolean
purple_libpwmd_clear_password_finish (PurpleCredentialProvider *provider,
				      GAsyncResult *result, GError **error)
{
  g_return_val_if_fail (PURPLE_IS_CREDENTIAL_PROVIDER (provider), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  PurpleLibPwmd *tp = PURPLE_LIBPWMD (provider);
  pwmd_close (tp->pwm);
  tp->pwm = NULL;
  return g_task_propagate_boolean (G_TASK (result), error);
}

/******************************************************************************
 * GObject Implementation
 *****************************************************************************/
static void
purple_libpwmd_init (PurpleLibPwmd *libpwmd)
{
  pwmd_init ();
}

static void
purple_libpwmd_class_init (PurpleLibPwmdClass *klass)
{
  PurpleCredentialProviderClass *provider_class = NULL;

  provider_class = PURPLE_CREDENTIAL_PROVIDER_CLASS (klass);
  provider_class->read_password_async = purple_libpwmd_read_password_async;
  provider_class->read_password_finish = purple_libpwmd_read_password_finish;
  provider_class->write_password_async = purple_libpwmd_write_password_async;
  provider_class->write_password_finish = purple_libpwmd_write_password_finish;
  provider_class->clear_password_async = purple_libpwmd_clear_password_async;
  provider_class->clear_password_finish = purple_libpwmd_clear_password_finish;
}

static void
purple_libpwmd_class_finalize (PurpleLibPwmdClass *klass)
{
}

/******************************************************************************
 * API
 *****************************************************************************/
static PurpleCredentialProvider *
purple_libpwmd_new ()
{
  return g_object_new (PURPLE_TYPE_LIBPWMD,
		       "id", LIBPWMD_ID,
		       "name", _(LIBPWMD_NAME),
		       "description", _(LIBPWMD_DESCRIPTION),
                       NULL);
}

/******************************************************************************
 * Plugin Exports
 *****************************************************************************/
static GPluginPluginInfo *
libpwmd_query (G_GNUC_UNUSED GError **error)
{
  const gchar *const authors[] = {
    "Ben Kibbey <bjk@luxsci.net>",
    NULL
  };

  return GPLUGIN_PLUGIN_INFO (purple_plugin_info_new (
                          "id", "keyring-" LIBPWMD_ID,
                          "name", LIBPWMD_NAME,
                          "version", LIBPWMD_PLUGIN_VERSION,
			  "category", N_("Keyring"),
                          "summary", "Libpwmd Keyring Plugin",
                          "description", N_("Adds support for using libpwmd as a keyring."),
                          "authors", authors,
                          "website", LIBPWMD_URL,
                          "abi-version", PURPLE_ABI_VERSION,
			  "flags", PURPLE_PLUGIN_INFO_FLAGS_INTERNAL|PURPLE_PLUGIN_INFO_FLAGS_AUTO_LOAD,
                          "settings-schema", SETTINGS_SCHEMA_ID,
                          NULL
  ));
}

static gboolean
libpwmd_load (GPluginPlugin *plugin, GError **error)
{
  PurpleCredentialManager *manager = NULL;
  gboolean ret = FALSE;

  purple_libpwmd_register_type (G_TYPE_MODULE (plugin));
  manager = purple_credential_manager_get_default ();
  instance = purple_libpwmd_new ();

  ret = purple_credential_manager_add (manager, instance, error);
  if (!ret)
    g_clear_object (&instance);

  return ret;
}

static gboolean
libpwmd_unload (G_GNUC_UNUSED GPluginPlugin *plugin,
		G_GNUC_UNUSED gboolean shutdown, GError **error)
{
  PurpleCredentialManager *manager = NULL;
  gboolean ret = FALSE;

  manager = purple_credential_manager_get_default ();
  ret = purple_credential_manager_remove (manager, instance, error);
  if (!ret)
    return ret;

  g_clear_object (&instance);
  return TRUE;
}

G_BEGIN_DECLS
GPLUGIN_NATIVE_PLUGIN_DECLARE (libpwmd)
G_END_DECLS
